#!/bin/python3
# Stable Diffusion Common Utilities for automatic1111's webui
# really, really dirty, just used for quick hacking
import base64
import copy
import datetime
import io
import json
import re
import time

import ipdb as pdb
import numpy as np
import PIL.Image
import PIL.PngImagePlugin
import requests

import cmutils.cmutils as cmu

base_url = "http://localhost:7860"


class StableDiffusion:
    @staticmethod
    def save_image(b64img, output_file):
        image = PIL.Image.open(io.BytesIO(base64.b64decode(b64img.split(",", 1)[0])))

        # Get Metadata
        png_payload = {"image": "data:image/png;base64," + b64img}
        response2 = requests.post(url=f"http://127.0.0.1:7860/sdapi/v1/png-info", json=png_payload)
        pnginfo = PIL.PngImagePlugin.PngInfo()
        pnginfo.add_text("parameters", response2.json().get("info"))

        image.save(output_file, pnginfo=pnginfo)

    @staticmethod
    def txt2img(
        output_files: [str],
        request_body: dict,
        append_timings=False,
        skip_existing=True,
    ):
        assert (not append_timings) or (append_timings and not skip_existing)

        # Create Dir(s) & Check if File(s) Exist
        for output_file in output_files:
            cmu.ShellUtils.mkparentsdir(output_file)
            if skip_existing and cmu.ShellUtils.file_exists(output_file):
                return True

        # Time Request
        req_start = time.time()
        x = requests.post(url=f"{base_url}/sdapi/v1/txt2img", json=request_body)
        req_end = time.time()
        req_time = req_end - req_start
        if append_timings:
            req_time = f"{req_time:.2f}"
            output_files = [
                output_file[:-4] + f"-t_{req_time}s" + output_file[-4:] for output_file in output_files
            ]

        # Report if errors
        if x.status_code != 200:
            print(x.status_code, x.reason, x.content)
            # import ipdb as pdb
            # pdb.set_trace()

        # Save Images
        u = json.loads(x.content)
        n_images = len(u["images"])

        assert n_images == len(output_files)

        for i in range(n_images):
            StableDiffusion.save_image(u["images"][i], output_files[i])

    @staticmethod
    def img2img(
        input_file: str,
        output_files: [str],
        request_body: dict,
        append_timings=False,
        skip_existing=True,
    ):
        assert (not append_timings) or (append_timings and not skip_existing)

        # Create Dir(s) & Check if File(s) Exist
        for output_file in output_files:
            cmu.ShellUtils.mkparentsdir(output_file)
            if skip_existing and cmu.ShellUtils.file_exists(output_file):
                return True

        # Add image to request_body
        request_body["init_images"] = [ImageUtilities.img_to_b64(input_file)]

        # Time Request
        req_start = time.time()
        x = requests.post(url=f"{base_url}/sdapi/v1/txt2img", json=request_body)
        req_end = time.time()
        req_time = req_end - req_start
        if append_timings:
            req_time = f"{req_time:.2f}"
            output_files = [
                output_file[:-4] + f"-t_{req_time}s" + output_file[-4:] for output_file in output_files
            ]

        # Report if errors
        if x.status_code != 200:
            print(x.status_code, x.reason, x.content)
            # import ipdb as pdb
            # pdb.set_trace()

        # Get Images
        u = json.loads(x.content)
        n_images = len(u["images"])

        # Save Images
        u = json.loads(x.content)
        n_images = len(u["images"])

        assert n_images == len(output_files)

        for i in range(n_images):
            StableDiffusion.save_image(u["images"][i], output_files[i])

    @staticmethod
    def upscale_image(
        input_file,
        output_file,
        upscaling_resize=2,
        upscaler_1="None",
        upscaler_2="None",
        extras_upscaler_2_visibility=0,
    ):
        if cmu.ShellUtils.file_exists(output_file):
            return False

        img = PIL.Image.open(input_file)

        endpoint = ""
        payload = {
            "resize_mode": 0,
            "show_extras_results": True,
            "gfpgan_visibility": 0,
            "codeformer_visibility": 0,
            "codeformer_weight": 0,
            "upscaling_resize": upscaling_resize,
            "upscaling_resize_w": int(img.width * upscaling_resize),
            "upscaling_resize_h": int(img.height * upscaling_resize),
            "upscaling_crop": True,
            "upscaler_1": upscaler_1,
            "upscaler_2": upscaler_2,
            "extras_upscaler_2_visibility": extras_upscaler_2_visibility,
            "upscale_first": False,
            "image": ImageUtilities.img_to_b64(input_file),
        }

        response = requests.post(url=f"{base_url}/sdapi/v1/extra-single-image", json=payload)

        r = response.json()
        # print()
        # print("___________________Payload___________________")
        # print(cmu.PrintUtils.basic_print(payload))
        # print("____________________________________________________________")
        # print("___________________Response___________________")
        # print(cmu.PrintUtils.basic_print(response.status_code))
        # print(cmu.PrintUtils.basic_print(response.json()))
        # print("____________________________________________________________")

        Data = PIL.Image.open(io.BytesIO(base64.b64decode(r["image"].split(",", 1)[0])))
        Data.save(output_file)
        return True

    @staticmethod
    def img_2_img_removebg(
        input_file,
        output_file,
        request_body=None,
        append_timings=False,
        skip_existing=True,
        cn_opts={},
    ):
        if not request_body:
            request_body = {
                "input_image": "",
                "model": "u2net",
                "return_mask": False,
                "alpha_matting": False,
                "alpha_matting_foreground_threshold": 240,
                "alpha_matting_background_threshold": 10,
                "alpha_matting_erode_size": 10,
            }
        # Create Dir
        cmu.ShellUtils.mkparentsdir(output_file)

        # Add image to request_body
        request_body["input_image"] = ImageUtilities.img_to_b64(input_file)

        # Check if it exists
        if skip_existing:
            parent_folder = output_file[: output_file.rfind("/")]
            file_name = output_file[output_file.rfind("/") + 1 : -4]
            files = cmu.ShellUtils.ls(parent_folder)
            for f in files:
                if f[: max(f.find("-t_"), len(file_name))] == file_name:
                    return True

        # Time Request
        req_start = time.time()
        x = requests.post(url=f"{base_url}/rembg", json=request_body)
        req_end = time.time()
        req_time = req_end - req_start
        if append_timings:
            req_time = f"{req_time:.2f}"
            output_file = output_file[:-4] + f"-t_{req_time}s" + output_file[-4:]

        # Report if errors
        if x.status_code != 200:
            print(x.status_code, x.reason, x.content)
            # import ipdb as pdb
            # pdb.set_trace()

        # Get Images
        u = json.loads(x.content)
        StableDiffusion.save_image(u["image"], output_file)
        # cmu.PrintUtils.print_model(u)
        # pdb.set_trace()
        # n_images = len(u['images'])

        # if n_images== 1: StableDiffusion.save_image(u['image'], output_file)
        # elif n_images == 2 \
        #     and "alwayson_scripts" in request_body \
        #     and "controlnet" in request_body["alwayson_scripts"]:
        #     StableDiffusion.save_image(u['images'][0], output_file)
        #     if "save_cn" in cn_opts \
        #         and cn_opts["save_cn"]: StableDiffusion.save_image(u['images'][1], f"{output_file[:-4]}_cn.png")
        # else:
        #     for i in range(n_images): StableDiffusion.save_image(u['images'][i], f"{output_file[:-4]}_{i}.png")

        # # Save Image
        # for i in u['images']: pass


# Control net: https://github.com/Mikubill/sd-webui-controlnet/wiki/API
class ControlNet:
    @staticmethod
    def canny_img2img(
        input_file: str,
        output_files: [str],
        request_body: dict,
        append_timings=False,
        skip_existing=True,
        control_mode="Balanced",
        weight=1,
        threshold_a=100,
        threshold_b=200,
    ):
        request_body["alwayson_scripts"] = {
            "controlnet": {
                "args": [
                    {
                        "enabled": True,
                        "module": "canny",
                        "model": "control_v11p_sd15_canny [d14c016b]",
                        "weight": weight,
                        "resize_mode": "Crop and Resize",
                        "threshold_a": threshold_a,
                        "threshold_b": threshold_b,
                        "control_mode": control_mode,
                        "processor_res": 512,
                    }
                ]
            }
        }

        return StableDiffusion.img2img(
            input_file=input_file,
            output_files=output_files,
            request_body=request_body,
            append_timings=append_timings,
            skip_existing=skip_existing,
        )


class CLIPInterrogator:
    @staticmethod
    def get_prompt_for_image(
        input_file, output_file=None, clip_model_name="ViT-L-14/openai", mode="fast"
    ) -> str:
        request_body = {
            "image": ImageUtilities.img_to_b64(input_file),
            "clip_model_name": clip_model_name,
            "mode": mode,
        }

        response = requests.post(url=f"http://127.0.0.1:7860/interrogator/prompt", json=request_body)

        prompt = response.json().get("prompt")
        # pdb.set_trace()
        if output_file:
            cmu.ShellUtils.write_text_to_file(output_file, prompt)
        return prompt


class Utilities:
    @staticmethod
    def decode_parameters(parameters: str) -> dict:
        parameters = parameters.replace("|", ",")
        spl1 = parameters.find("\nNegative prompt: ")
        spl2 = parameters.find("\nSteps: ", spl1)
        prompt = parameters[:spl1].strip()
        negative_prompt = parameters[spl1:spl2].strip()
        options = parameters[spl2:].strip()
        # prompt, negative_prompt, options = parameters.split("\n")
        negative_prompt = negative_prompt.replace("Negative prompt: ", "")
        request = {}
        request["prompt"] = prompt
        request["negative_prompt"] = negative_prompt

        def decode_parameters_helper(key, str):
            st = str.find(key)
            ret = str[st + len(key) :]
            if "," in ret:
                ret = ret[: ret.find(",")]
            return ret.strip()

        request["steps"] = decode_parameters_helper("Steps: ", options)
        request["seed"] = decode_parameters_helper("Seed: ", options)
        request["sampler_index"] = decode_parameters_helper("Sampler: ", options)
        request["batch_size"] = 1
        request["cfg_scale"] = decode_parameters_helper("CFG scale: ", options)
        request["width"], request["height"] = decode_parameters_helper("Size:", options).split("x")
        request["override_settings"] = {}

        mhash = decode_parameters_helper("Model hash:", options)
        model = decode_parameters_helper("Model:", options)
        request["override_settings"]["sd_model_checkpoint"] = f"{model}.safetensors [{mhash}]"
        # cmu.PrintUtils.basic_print(request)
        return request

    # returns timestamp:str if success, None if otherwise
    @staticmethod
    def add_to_cache(request_body: dict) -> str:
        key = f"sd_req: {cmu.MiscUtils.hash(json.dumps(request_body))}"

        timestamp = f"{datetime.datetime.utcnow()} {datetime.datetime.utcnow().timestamp()}"

        if key in cmu.Cache():
            return None
        cmu.Cache()[key] = timestamp.encode("utf-8")

        return timestamp

    # cache_timestamp_request will return (True, ...) if in cache (False, ...) if not in cache
    # If cache item exists
    @staticmethod
    def in_cache(request_body: dict) -> (bool, str):
        return f"sd_req: {cmu.MiscUtils.hash(json.dumps(request_body))}" in cmu.Cache()

    # cache_timestamp_request will return (True, ...) if in cache (False, ...) if not in cache
    # If cache item exists
    @staticmethod
    def get_cache_timestamp(request_body: dict) -> str:
        key = f"sd_req: {cmu.MiscUtils.hash(json.dumps(request_body))}"
        return cmu.Cache()[key].decode("utf-8")


class ImageUtilities(Utilities):
    @staticmethod
    def img_to_b64(filename):
        encoded_image = ""
        with open(filename, "rb") as file:
            image_data = file.read()
            encoded_image = base64.b64encode(image_data).decode("utf-8")

        return encoded_image

    @staticmethod
    def get_request_for_file(filename: str):
        request = dict()
        img = PIL.Image.open(filename)
        if "parameters" in img.info:
            parameters = img.info["parameters"]
            try:
                request = ImageUtilities.decode_parameters(parameters)
            except:
                pass
        request["width"] = img.width
        request["height"] = img.height
        return request


class MiscUtils(Utilities):
    @staticmethod
    def try_range(
        output_dir,
        st,
        en,
        sk,
        key,
        orig_request_body,
        append_timings=True,
        skip_existing=True,
    ):
        print(f"Trying range for key:{key} | st:{st}, en:{en}, sk:{sk}", flush=True)

        st_time = time.time()
        whole_nums = (
            abs(float(int(st)) - st) <= 0.000001
            and abs(float(int(en)) - en) <= 0.000001
            and abs(float(int(sk)) - sk) <= 0.000001
        )
        for j in np.arange(st, en, sk):
            i = f"{j:.3f}"
            if whole_nums:
                i = int(j)
            print(f"{key}:{i} | ", end="")
            request_body = copy.deepcopy(orig_request_body)
            request_body[key] = i
            req_start = time.time()
            hashed = cmu.MiscUtils.hash(str(orig_request_body))
            skipped = StableDiffusion.txt2img(
                request_body,
                f"{output_dir}/{hashed}/{key}/{i}.png",
                append_timings=append_timings,
                skip_existing=skip_existing,
            )
            req_end = time.time()
            if not skipped:
                print(f"{req_end - req_start}s")
            else:
                print("skipped")

        en_time = time.time()
        print(f"Total time: {en_time - st_time}s")

    @staticmethod
    def try_string_range(
        output_dir,
        strings,
        key,
        orig_request_body,
        append_timings=True,
        skip_existing=True,
    ):
        print(f"Trying {len(strings)} strings for key:{key}", flush=True)
        st_time = time.time()
        for i in strings:
            print(f"{key}:{i} | ", end="")
            request_body = copy.deepcopy(orig_request_body)
            request_body[key] = i
            req_start = time.time()
            hashed = cmu.MiscUtils.hash(str(orig_request_body))
            skipped = StableDiffusion.txt2img(
                request_body,
                f"{output_dir}/{hashed}/{key}/{i}.png",
                append_timings=append_timings,
                skip_existing=skip_existing,
            )
            req_end = time.time()
            if not skipped:
                print(f"{req_end - req_start}s")
            else:
                print("skipped")

        en_time = time.time()
        print(f"Total time: {en_time - st_time}s")


class SimpleToken:
    def is_lora(self):
        return self.lora != None

    def is_weighted(self):
        return self.weight != None

    def get_weight(self):
        return float(self.weight)

    def set_weight(self, weight):
        self.weight = f"{float(weight):.5f}".rstrip("0")

    def inc_weight(self, inc: float):
        self.weight = f"{(float(self.weight)+inc):.5f}".rstrip("0")

    def dec_weight(self, dec: float):
        self.weight = f"{(float(self.weight)-dec):.5f}".rstrip("0")

    def get_content(self):
        return self.content

    def get_lora(self):
        return self.lora

    def get_n_paranthesis(self):
        return self.n_paranthesis

    def get_token(self):
        ret = ""
        for i in range(self.n_paranthesis):
            ret += "("
        if self.is_lora():
            ret += "<lora:" + self.lora + ":" + self.weight + ">"
        else:
            ret += self.content
            if self.is_weighted():
                ret += ":" + self.weight
        for i in range(self.n_paranthesis):
            ret += ")"
        return ret

    def __init__(self, token: str):
        self.lora = None
        self.weight = None
        self.n_paranthesis = 0

        self.original_token = token
        self.content = token.strip()

        if len(self.content) == 0:
            return

        while self.content[0] == "(" and self.content[-1] == ")":
            self.n_paranthesis += 1
            self.content = self.content[1:-1]

        if self.content[0] == "<" and self.content[-1] == ">" and "lora" in self.content:
            _, self.lora, self.weight = self.content[1:-1].split(":")
            self.content = self.lora
            return

        elif ":" in self.content:
            self.content, self.weight = self.content.split(":")

    def __lt__(self, other):
        return self.content < other.content


class SimplePrompt:
    delims = ["\n", ","]

    @staticmethod
    def get_token_end(prompt: str, st_idx) -> int:
        tlen = 1
        while st_idx + tlen < len(prompt) and prompt[st_idx + tlen] not in SimplePrompt.delims:
            # print(f"{st_idx}: {tlen}, {prompt[st_idx + tlen]}")
            tlen += 1
        return min(len(prompt), st_idx + tlen)

    @staticmethod
    def cleanup_prompt(prompt: str) -> str:
        while "  " in prompt:
            prompt = prompt.replace("  ", " ")
        prompt = prompt.replace("\r", "")
        prompt = prompt.replace("\n", ",\n")
        prompt = prompt.replace("\n,", "\n")
        while "\n " in prompt:
            prompt = prompt.replace("\n ", "\n")
        prompt = prompt.replace("\n,\n", "\n")
        while "\n\n" in prompt:
            prompt = prompt.replace("\n\n", "\n")
        while ",," in prompt:
            prompt = prompt.replace(",,", ",")
        if len(prompt) == 0:
            return ""
        while prompt[0] == " " or prompt[0] in SimplePrompt.delims:
            prompt = prompt[1:]
        while prompt[-1] in SimplePrompt.delims:
            prompt = prompt[:-1]
        return prompt

    @staticmethod
    def strip_lora(prompt: str) -> str:
        # print("Stripping:", prompt)
        if "lora:" not in prompt:
            return prompt

        new_prompt = ""

        tok_start = 0
        while tok_start < len(prompt):
            # Skip Delims
            if prompt[tok_start] in SimplePrompt.delims:
                new_prompt += prompt[tok_start]
                tok_start += 1
                continue

            tok_end = SimplePrompt.get_token_end(prompt, tok_start)

            # print("SL look tok:", prompt[tok_start:tok_end])
            token = SimpleToken(prompt[tok_start:tok_end])

            if not token.is_lora():
                new_prompt += prompt[tok_start:tok_end]

            # Add seperator
            # if tok_end < len(prompt): new_prompt += prompt[tok_end]

            # Start Next Token
            tok_start = tok_end

        return SimplePrompt.cleanup_prompt(new_prompt)

    @staticmethod
    def remove_token_regexes_from_prompt(regexes: [], prompt: str) -> str:
        new_prompt = ""
        regexes = [re.compile(regex) for regex in regexes]

        tok_start = 0
        while tok_start < len(prompt):
            # Skip Delims
            if prompt[tok_start] in SimplePrompt.delims:
                new_prompt += prompt[tok_start]
                tok_start += 1
                continue

            tok_end = SimplePrompt.get_token_end(prompt, tok_start)

            # print("SL look tok:", prompt[tok_start:tok_end])
            token = SimpleToken(prompt[tok_start:tok_end]).get_token()

            skip = False
            for regex in regexes:
                # print(regex, "|", token, "|", re.search(regex,token))
                if re.search(regex, token):
                    skip = True
                    break

            if not skip:
                new_prompt += prompt[tok_start:tok_end]

            # Add seperator
            # if tok_end < len(prompt): new_prompt += prompt[tok_end]

            # Start Next Token
            tok_start = tok_end

        return SimplePrompt.cleanup_prompt(new_prompt)

    @staticmethod
    def remove_token_regex_from_prompt(token: str, prompt: str) -> str:
        return SimplePrompt.remove_token_regex_from_prompt([token], prompt)

    @staticmethod
    def remove_tokens_from_prompt(tokens: [str], prompt: str) -> str:
        return SimplePrompt.remove_token_regexes_from_prompt(
            ["^" + re.escape(token) + "$" for token in tokens], prompt
        )
        # tokens = [SimpleToken(token).get_token() for token in tokens]
        # new_prompt = ""

        # tok_start = 0
        # while tok_start < len(prompt):
        #     #Skip Delims
        #     if prompt[tok_start] in SimplePrompt.delims:
        #         new_prompt += prompt[tok_start]
        #         tok_start += 1
        #         continue

        #     tok_end = SimplePrompt.get_token_end(prompt, tok_start)

        #     # print("SL look tok:", prompt[tok_start:tok_end])
        #     token = SimpleToken(prompt[tok_start:tok_end])

        #     if not token.get_token() in tokens:
        #         new_prompt += prompt[tok_start:tok_end]

        #     # Add seperator
        #     # if tok_end < len(prompt): new_prompt += prompt[tok_end]

        #     # Start Next Token
        #     tok_start = tok_end

        # return SimplePrompt.cleanup_prompt(new_prompt)

    @staticmethod
    def remove_token_from_prompt(token: str, prompt: str) -> str:
        return SimplePrompt.remove_tokens_from_prompt([token], prompt)

    @staticmethod
    def token_in_prompt(token: str, prompt: str) -> bool:
        token = SimpleToken(token)
        tok_start = 0
        while tok_start < len(prompt):
            # Skip Delims
            if prompt[tok_start] in SimplePrompt.delims:
                tok_start += 1
                continue

            tok_end = SimplePrompt.get_token_end(prompt, tok_start)
            curr_token = SimpleToken(prompt[tok_start:tok_end])

            if token.get_token() == curr_token.get_token():
                return True
            tok_start = tok_end

        return False

    @staticmethod
    def merge_prompts(prompt1: str, prompt2: str) -> str:
        prompt1 += ",\n"
        tok_start = 0
        while tok_start < len(prompt2):
            # Skip Delims
            if prompt2[tok_start] in SimplePrompt.delims:
                prompt1 += prompt2[tok_start]
                tok_start += 1
                continue

            tok_end = SimplePrompt.get_token_end(prompt2, tok_start)

            # print("SL look tok:", prompt[tok_start:tok_end])
            token = SimpleToken(prompt2[tok_start:tok_end])

            if not SimplePrompt.token_in_prompt(token.get_token(), prompt1):
                prompt1 += prompt2[tok_start:tok_end]

            # Add seperator
            # if tok_end < len(prompt): new_prompt += prompt[tok_end]

            # Start Next Token
            tok_start = tok_end

        return SimplePrompt.cleanup_prompt(prompt1)
