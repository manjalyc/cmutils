from PIL import Image
from PyQt5.QtCore import QRectF, Qt
from PyQt5.QtGui import (QColor, QGuiApplication, QImage, QPainter, QPixmap,
                         QWheelEvent)
from PyQt5.QtWidgets import (QApplication, QGraphicsPixmapItem, QGraphicsScene,
                             QGraphicsView)

global qapplication_image_app
qapplication_image_app = None


def app_init():
    global qapplication_image_app
    if qapplication_image_app is None:
        qapplication_image_app = QApplication([])
    viewer = ImageViewer(None)
    viewer.show()
    return viewer


class ImageViewer(QGraphicsView):
    def __init__(self, image=None):
        super().__init__()
        self.setRenderHint(QPainter.Antialiasing)
        self.setRenderHint(QPainter.SmoothPixmapTransform)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)

        self.scene = QGraphicsScene(self)
        self.setScene(self.scene)

        # Set the background color to grey
        self.scene.setBackgroundBrush(QColor(200, 200, 200))  # Light grey background

        self.item = None
        self.setDragMode(QGraphicsView.ScrollHandDrag)  # Enable drag mode

        # Get screen geometry and set window size
        screen_geometry = QGuiApplication.primaryScreen().geometry()
        self.resize(screen_geometry.width() // 2, screen_geometry.height() // 4)
        self.move(
            (screen_geometry.width() - self.width()) // 2,
            (screen_geometry.height() - self.height()) // 2,
        )

        if image:
            self.update_image(image)

    def fit_image_to_window(self):
        if self.item:
            # Fit the image to the window
            self.setSceneRect(self.item.boundingRect())
            self.fitInView(self.item, Qt.KeepAspectRatio)
            self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)

    def pil_image_to_pixmap(self, pil_image):
        # Convert PIL image to QImage
        if pil_image.mode != "RGB":
            pil_image = pil_image.convert("RGB")
        qimage = QImage(
            pil_image.tobytes(),
            pil_image.width,
            pil_image.height,
            pil_image.width * 3,
            QImage.Format_RGB888,
        )
        return QPixmap.fromImage(qimage)

    def set_title(self, windowtitle:str):
        # print("SEtting title", windowtitle)
        self.setWindowTitle(windowtitle)

    # image = pil, qpixmap, or path
    def update_image(self, image, title="Python Image Viewer"):
        # print(title)
        self.set_title(title)

        # Remove the old image if it exists
        if self.item:
            self.scene.removeItem(self.item)

        if isinstance(image, str):
            image = Image.open(image)

        # Convert PIL Image to QPixmap
        if isinstance(image, Image.Image):
            pixmap = self.pil_image_to_pixmap(image)
        elif isinstance(image, QPixmap):
            pixmap = image
        else:
            print(image)
            raise ValueError("Unsupported image format. Provide a PIL Image, Filepath, or QPixmap.")

        self.item = QGraphicsPixmapItem(pixmap)
        self.scene.addItem(self.item)

        # Adjust the view to fit the new image
        self.fit_image_to_window()

    def wheelEvent(self, event: QWheelEvent):
        # Zoom in and out with mouse wheel
        factor = 1.2
        if event.angleDelta().y() < 0:
            factor = 1.0 / factor
        self.scale(factor, factor)
