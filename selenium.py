from __future__ import annotations

import io
import signal
import time
# from chromedriver_py import binary_path
import urllib

import psutil
import selenium
from PIL import Image
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select, WebDriverWait

import cmutils.cmutils as cmu

try:
    import undetected_chromedriver
except:
    pass

try:
    from webdriver_manager.chrome import ChromeDriverManager

except:
    pass

global exit_handler_driver


class ExtendedDriver:
    def __init__(self):
        self.pid = None

    @staticmethod
    def register_exit_handler(driver):
        global exit_handler_driver
        exit_handler_driver = driver
        signal.signal(signal.SIGINT, ExtendedDriver.exit_handler)
        signal.signal(signal.SIGTERM, ExtendedDriver.exit_handler)

    @staticmethod
    def exit_handler(sig, frame):
        global exit_handler_driver
        print(f"[cmutils.selenium] ExtendedDriver handling sig {sig}")
        print(f"[cmutils.selenium] {exit_handler_driver.force_quit()}")
        signal.default_int_handler(sig, frame)

    def screenshot_and_dump_page_source(
        self, screenshot_file="selenium.png", pagesource_file="pagesource.html"
    ):
        driver = self
        screenshot_file = cmu.ShellUtils.get_absolute_path(screenshot_file)
        pagesource_file = cmu.ShellUtils.get_absolute_path(pagesource_file)
        cmu.ShellUtils.mkparentsdir(screenshot_file)
        cmu.ShellUtils.mkparentsdir(pagesource_file)

        screenshot = driver.get_screenshot_as_png()

        Image.open(io.BytesIO(screenshot)).save(screenshot_file)
        cmu.ShellUtils.write_text_to_file(pagesource_file, driver.page_source)

    def force_quit(self):
        driver = self
        driver_pid = driver.service.process.pid
        # import ipdb as pdb
        # pdb.set_trace()
        driver.quit()

        try:
            psutil.Process(driver_pid).terminate()
        except psutil.NoSuchProcess:
            return (0, "Natural Exit")

        time.sleep(5)

        try:
            psutil.Process(driver_pid).kill()
        except psutil.NoSuchProcess:
            return (1, "Terminated")

        return (2, f"Attempted Force Kill for PID: {driver_pid}")

    def xpath_exists(self, xpath):
        driver = self
        try:
            driver.find_element(By.XPATH, xpath)
        except NoSuchElementException:
            return False
        return True

    def wait_for_xpath(self, xpath, timeout: int):
        try:
            elements = WebDriverWait(self, timeout).until(
                EC.presence_of_all_elements_located((By.XPATH, xpath))
            )
        except:
            pass
        return self.xpath_exists(xpath)

    def get_elements(self, xpath):
        driver = self
        return driver.find_elements(By.XPATH, xpath)

    def get_element(self, xpath):
        return self.get_elements(xpath)[0]

    def get_text(self, xpath):
        driver = self
        element = self.get_element(xpath)
        return element.text

    def send_string(self, str):
        driver = self
        actions = ActionChains(driver)
        actions.send_keys(str)
        actions.perform()

    def ctrl_a_bkspace(self):
        driver = self
        actions = ActionChains(driver)
        actions.key_down(Keys.CONTROL)
        actions.send_keys("a")
        actions.key_up(Keys.CONTROL)
        actions.perform()
        actions.send_keys(Keys.BACKSPACE).perform()

    def SHIFT_ENTER(self):
        ActionChains(self).key_down(Keys.SHIFT).key_down(Keys.ENTER).key_up(Keys.SHIFT).key_up(
            Keys.ENTER
        ).perform()

    def ENTER(self):
        ActionChains(self).key_down(Keys.ENTER).key_up(Keys.ENTER).perform()

    def RIGHT(self):
        actions = ActionChains(self)
        actions.key_down(Keys.RIGHT)
        actions.key_up(Keys.RIGHT)
        actions.perform()

    def find_xpath_elems(self, xpath, timeout=0):
        driver = self
        try:
            elements = WebDriverWait(driver, timeout).until(
                EC.presence_of_all_elements_located((By.XPATH, xpath))
            )
            if len(elements) == 0:
                print(f"[CRIT] Did Not Locate after {timeout}s timeout: {xpath}")
            return elements
        except Exception as e:
            return []

    def find_xpath_elem(self, xpath, timeout=0):
        return self.find_xpath_elems(xpath, timeout=timeout)[0]

    def find_and_click_element(self, element, timeout=0):
        try:
            WebDriverWait(self, timeout).until(EC.element_to_be_clickable(element)).click()
        except TimeoutException:
            return False
        return True

    def find_and_click(self, xpath, timeout=0):
        return self.find_and_click_element((By.XPATH, xpath), timeout=timeout)
        # element = self.find_xpath_elem(xpath)

        # import ipdb
        # ipdb.set_trace()
        element = WebDriverWait(self, timeout).until(EC.element_to_be_clickable()).click()

        # try:
        #     actions = ActionChains(self)
        #     actions.move_to_element(element)
        #     actions.perform()
        # except:
        #     pass

        # element.click()

    # def find_and_click(self, xpath, timeout=0):
    #     element = self.find_xpath_elem(xpath)

    #     WebDriverWait(self, timeout).until(
    #         EC.element_to_be_clickable(element)
    #     )

    #     actions = ActionChains(self)
    #     actions.move_to_element(element)
    #     actions.click()
    #     actions.perform()

    def get_all_href_uris(self):
        return self.find_xpath_elems("//a[@href]")

    def get_all_href_uris_set(self):
        uris = set()
        for uri in self.get_all_href_uris():
            try:
                if not self.is_stale(uri):
                    uris.add(uri.get_attribute("href"))
            except Exception as e:
                pass
            finally:
                pass
        return uris

    def move_to_end(self):
        actions = ActionChains(self)
        actions.key_down(Keys.END)
        actions.key_up(Keys.END)
        actions.perform()

    def is_stale(self, element):
        try:
            element.is_enabled()
            return False
        finally:
            return False

    def new_tab(self, activate=True):
        driver = self
        driver.switch_to.new_window()
        # driver.execute_script("window.open('about:blank');")
        # if activate:
        #     for window_handle in driver.window_handles:
        #         driver.switch_to.window(window_handle)
        #         if driver.current_url == "about:blank":
        #             return

    def activate_tab_n(self, n):
        driver = self
        wh = driver.window_handles[n]
        driver.switch_to.window(wh)

    def num_tabs(self):
        return len(self.window_handles)

    def current_handles(self):
        return set(self.window_handles)

    def close_all_but_first_tab(self):
        while len(self.window_handles) > 1:
            self.activate_tab_n(1)
            self.close()
        self.activate_tab_n(0)

    def ctrl_click_close(self, elem, timeout=3):
        original_handle = self.current_window_handle

        curr_tabs = len(self.window_handles)

        prior_tabs = set()
        for handle in self.window_handles:
            prior_tabs.add(handle)

        try:
            ActionChains(self).key_down(Keys.CONTROL).click(elem).key_up(Keys.CONTROL).perform()
        except Exception:
            print("CTRL CLICK DOWNLOAD ERROR")
            pass

        i = 0
        while curr_tabs == len(self.window_handles) and i * 0.1 < timeout:
            i += 1
            time.sleep(0.1)

        # Identify new tabs
        for handle in self.window_handles:
            if handle in prior_tabs:
                continue
            else:
                self.switch_to.window(handle)
                # uri=self.current_url
                # uri=uri[:uri.find("?")]
                # uri=uri[uri.rfind("/")+1:]
                # print("saving", uri)

                # import ipdb as pdb
                # pdb.set_trace()

                self.close()

        self.switch_to.window(original_handle)

        # import ipdb as pdb
        # pdb.set_trace()


# class ChromeExtendedDriver(webdriver.Chrome, ExtendedDriver):
#     def __del__(self):
#         self.quit()


class FirefoxExtendedDriver(webdriver.Firefox, ExtendedDriver):
    def __del__(self):
        self.quit()

    @classmethod
    def quickstart(
        cls,
        download_dir=None,
        profile_dir=None,
        cache=True,
        headless=False,
        geckodriver_location=None,
    ) -> FirefoxExtendedDriver:
        # Set Directories for Profile and Downloads
        if not profile_dir:
            profile_dir = "./firefox-profile"
        if not download_dir:
            download_dir = "./firefox-downloads"
        profile_dir = cmu.ShellUtils.get_absolute_path(profile_dir)
        download_dir = cmu.ShellUtils.get_absolute_path(download_dir)
        cmu.ShellUtils.mkdir(profile_dir)
        cmu.ShellUtils.mkdir(download_dir)

        options = webdriver.FirefoxOptions()

        # Profile Options
        options.add_argument("-profile")
        options.add_argument(profile_dir)

        # Don't block popups (for new_tab)
        options.set_preference("dom.popup_maximum", 0)

        # Download Options
        options.set_preference("browser.download.dir", download_dir)
        options.set_preference("browser.download.useDownloadDir", True)
        options.set_preference("browser.download.folderList", 2)
        options.set_preference("browser.browser.download.manager.showWhenStarting", False)
        options.set_preference("browser.helperApps.neverAsk.saveToDisk", "image/jpeg,video/mp4")

        # Cache Options
        if not cache:
            options.set_preference("browser.cache.disk.enable", False)
            options.set_preference("browser.cache.memory.enable", False)
            options.set_preference("browser.cache.offline.enable", False)
            options.set_preference("network.http.use-cache", False)

        # Headless
        if headless:
            options.add_argument("--headless")

        if geckodriver_location:
            service = selenium.webdriver.firefox.service.Service(
                executable_path=cmu.ShellUtils.get_absolute_path(geckodriver_location)
            )
            return cls(options=options, service=service)

        driver = cls(options=options)
        ExtendedDriver.register_exit_handler(driver)
        return driver


class ChromeExtendedDriver(webdriver.Chrome, ExtendedDriver):
    def __del__(self):
        self.quit()

    @classmethod
    def quickstart(
        cls,
        download_dir=None,
        profile_dir=None,
        binary_location=None,
        headless=False,
    ) -> ChromeExtendedDriver:
        if not profile_dir:
            profile_dir = "./chrome-profile"
        profile_dir = cmu.ShellUtils.get_absolute_path(profile_dir)
        cmu.ShellUtils.mkdir(profile_dir)

        # svc = webdriver.ChromeService(executable_path=binary_path)
        options = webdriver.ChromeOptions()

        options.add_argument("--disable-popup-blocking")
        options.add_argument(f"--user-data-dir={profile_dir}")
        # options.add_argument('--disable-extensions')
        # options.add_argument("-profile")
        # options.add_argument(profile)
        # options.add_argument("-user-agent")
        # options.add_argument(user_agent)

        if download_dir is not None:
            prefs = {}
            cmu.ShellUtils.mkdir(download_dir)
            prefs["profile.default_content_settings.popups"] = 0
            prefs["download.default_directory"] = cmu.ShellUtils.get_absolute_path(download_dir)
            prefs["profile.default_content_setting_values.automatic_downloads"] = 1
            options.add_experimental_option("prefs", prefs)

        # if binary_location is not None:
        #     options.binary_location = cmu.ShellUtils.get_absolute_path(binary_location)

        if headless:
            options.add_argument("--headless=new")
            # options.add_argument("--window-size=1840,1080")
            options.add_argument("start-maximized")

        driver = None
        driver = cls(options=options, service=ChromeService())
        ExtendedDriver.register_exit_handler(driver)
        return driver


class ChromeAutoExtendedDriver(webdriver.Chrome, ExtendedDriver):

    @classmethod
    def quickstart(
        cls,
        download_dir=None,
        profile_dir=None,
        binary_location=None,
        headless=False,
    ) -> UndetectedChromeExtendedDriver:
        if not profile_dir:
            profile_dir = "./undetected-chrome-profile"
        profile_dir = cmu.ShellUtils.get_absolute_path(profile_dir)
        cmu.ShellUtils.mkdir(profile_dir)

        # svc = webdriver.ChromeService(executable_path=binary_path)
        options = webdriver.ChromeOptions()

        options.add_argument("--disable-popup-blocking")
        options.add_argument(f"--user-data-dir={profile_dir}")
        # options.add_argument('--disable-extensions')
        # options.add_argument("-profile")
        # options.add_argument(profile)
        # options.add_argument("-user-agent")
        # options.add_argument(user_agent)

        if download_dir is not None:
            prefs = {}
            cmu.ShellUtils.mkdir(download_dir)
            prefs["profile.default_content_settings.popups"] = 0
            prefs["download.default_directory"] = cmu.ShellUtils.get_absolute_path(download_dir)
            prefs["profile.default_content_setting_values.automatic_downloads"] = 1
            options.add_experimental_option("prefs", prefs)

        # if binary_location is not None:
        #     options.binary_location = cmu.ShellUtils.get_absolute_path(binary_location)

        if headless:
            options.add_argument("--headless=new")

        driver = cls(
            options=options, service=ChromeService(executable_path=ChromeDriverManager().install())
        )
        ExtendedDriver.register_exit_handler(driver)
        return driver


try:

    class UndetectedChromeExtendedDriver(undetected_chromedriver.Chrome, ExtendedDriver):
        def __del__(self):
            self.quit()

        @classmethod
        def quickstart(
            cls,
            download_dir=None,
            profile_dir=None,
            binary_location=None,
            headless=False,
        ) -> UndetectedChromeExtendedDriver:
            if not profile_dir:
                profile_dir = "./undetected-chrome-profile"
            profile_dir = cmu.ShellUtils.get_absolute_path(profile_dir)
            cmu.ShellUtils.mkdir(profile_dir)

            # svc = webdriver.ChromeService(executable_path=binary_path)
            options = undetected_chromedriver.ChromeOptions()

            options.add_argument("--disable-popup-blocking")
            options.add_argument(f"--user-data-dir={profile_dir}")
            # options.add_argument('--disable-extensions')
            # options.add_argument("-profile")
            # options.add_argument(profile)
            # options.add_argument("-user-agent")
            # options.add_argument(user_agent)

            if download_dir is not None:
                prefs = {}
                cmu.ShellUtils.mkdir(download_dir)
                prefs["profile.default_content_settings.popups"] = 0
                prefs["download.default_directory"] = cmu.ShellUtils.get_absolute_path(download_dir)
                prefs["profile.default_content_setting_values.automatic_downloads"] = 1
                options.add_experimental_option("prefs", prefs)

            # if binary_location is not None:
            #     options.binary_location = cmu.ShellUtils.get_absolute_path(binary_location)

            if headless:
                options.add_argument("--headless=new")

            driver = cls(options=options, service=ChromeService())
            ExtendedDriver.register_exit_handler(driver)
            return driver

except:
    pass
