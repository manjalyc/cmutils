import os
import subprocess
import time
from collections import defaultdict

import regex as re


class Runner:
    @staticmethod
    def _run_command(cmd, cwd="."):
        print("[Runner] cmd:", cmd)
        print("[Runner] cwd:", cwd)
        subprocess.run(cmd, shell=True, cwd=cwd)

    @staticmethod
    def run_python_file(python_file, args=[]):
        file_directory = os.path.dirname(os.path.abspath(python_file))
        cmd = ["python", f"{python_file}"]
        cmd += args
        Runner._run_command(cmd, cwd=file_directory)
