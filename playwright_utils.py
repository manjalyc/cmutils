import ipdb as pdb
from playwright.sync_api import BrowserContext, Page, Playwright, sync_playwright
from playwright_stealth import stealth_sync

import cmutils.cmutils as cmu


class ExtendedPage(Page):
    def __init__(self, page: Page, playwright: Playwright):
        self.playwright = playwright
        self.__dict__.update(page.__dict__)

    def __del__(self):
        self.stop()

    def stop(self):
        self.playwright.stop()


def quickstart(
    browser_type="undetected_chromium",
    headless=False,
    download_dir="./downloads",
    persistent_profile_dir="./browser-profile",
    args=None,
    extension_paths=None,
) -> ExtendedPage:
    download_dir = cmu.ShellUtils.get_absolute_path(download_dir)
    persistent_profile_dir = cmu.ShellUtils.get_absolute_path(persistent_profile_dir)

    if args is None:
        args = []
    # disable navigator.webdriver:true flag
    args.append("--disable-blink-features=AutomationControlled")

    if extension_paths is not None:
        extension_paths = [cmu.ShellUtils.get_absolute_path(x) for x in extension_paths]
        args.append(f"--disable-extensions-except={','.join(extension_paths)},")
        for extension_path in extension_paths:
            args.append(f"--load-extension={extension_path}")

    p = sync_playwright().start()
    if browser_type == "undetected_chromium":
        browser = p.chromium.launch_persistent_context(
            user_data_dir=persistent_profile_dir,
            headless=headless,
            downloads_path=download_dir,
            args=args,
        )
    else:
        raise ValueError("Invalid browser type")

    if len(browser.pages) == 0:
        browser.new_page()
    page = browser.pages[0]

    stealth_sync(page.context)
    return ExtendedPage(page, playwright=p)
