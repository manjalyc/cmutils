import subprocess

from PIL import Image


class PDFtoImage:
    import fitz

    @staticmethod
    def to_images(input_pdf_path: str, output_folder: str, dpi=300, zpadding=4, verbose=False):
        doc = PDFtoImage.fitz.open(input_pdf_path)  # open document
        zoom = dpi / 72  # zoom factor, standard: 72 dpi
        magnify = PDFtoImage.fitz.Matrix(zoom, zoom)  # magnifies in x, resp. y direction
        for i, page in enumerate(doc):
            ii = str(i + 1).zfill(4)
            output_file = f"{output_folder}/page_{ii}.png"
            if verbose:
                print(f"> {output_file}")
            pix = page.get_pixmap(matrix=magnify)  # render page to an image
            pix.save(output_file)


class ToImage:
    @staticmethod
    def to_image(input_image_path: str, output_image_path: str, filetype):
        # Open the image file
        image_object = Image.open(input_image_path)

        # Save the image as a new file
        image_object.save(output_image_path, filetype)

    @staticmethod
    def to_png(input_image_path: str, png_file_path: str):
        return ToImage.to_image(input_image_path, png_file_path, "PNG")

    @staticmethod
    def extract_first_frame(video_path, output_image, silent=False):
        command = [
            "ffmpeg",
            "-y",
            "-i",
            video_path,
            "-vf",
            "select=eq(n\\,0)",
            "-q:v",
            "3",
            output_image,
        ]
        if not silent:
            subprocess.run(command, check=True)
        subprocess.run(command, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, check=True)
