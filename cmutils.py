import copy
import getpass
import hashlib
import os
import pathlib
import platform
import re
import shutil
import urllib.parse
import urllib.request

import requests

if platform.system() == "Windows":
    import msvcrt
else:
    import sys
    import termios
    import tty


class WebUtils:
    # Ignore
    @staticmethod
    def request_page(url: str):
        if Cache.is_active:
            if url in Cache():
                return Cache()[url]

        if Options.verbose_level >= 5:
            print(f"[DOWNLOADING]: {url}")

        binary_response = WebUtils.http_get(url, headers=None)

        if Cache.is_active:
            Cache()[url] = binary_response

        return binary_response

    # URL --> String
    @staticmethod
    def get_page_utf(url: str) -> str:
        response = WebUtils.request_page(url)
        return response.decode("UTF-8")

    # URL --> File
    @staticmethod
    def download_page(url: str, path: str) -> None:
        response = WebUtils.request_page(url)
        ShellUtils.mkparentsdir(path)
        open(path, "wb").write(response)

    @staticmethod
    def http_get(url: str, headers=dict()):
        request = urllib.request.Request(url)
        if len(headers) > 0:
            request = urllib.request.Request(url, headers=headers)
        binary_response = urllib.request.urlopen(request).read()
        return binary_response

    @staticmethod
    def http_get_json(url: str, auth=None, post_data=None, headers=None):
        response = requests.get(url, auth=auth, data=post_data, headers=headers)
        return response.json()

    @staticmethod
    def http_post_json(url: str, auth=None, post_data=None, headers=None):
        response = requests.post(url, auth=auth, data=post_data, headers=headers)
        # print(response.json())
        return response.json()

    # URLs --> Files
    @staticmethod
    def bulk_download(urls, paths) -> None:
        for url, path in zip(urls, paths):
            WebUtils.download_page(url, path)


class DataWrangler:
    # Cut Everything To the first appearance of a substring
    # Will Error out if substring not in str
    @staticmethod
    def cut_everything_before(input_string: str, substring: str) -> str:
        idx = input_string.index(substring)
        return input_string[idx:]

    # Cut Everything After the first appearance of a substring
    # Will Error out if substring not in str
    @staticmethod
    def cut_everything_after(input_string: str, substring: str, also_cut_substring=False) -> str:
        idx = input_string.index(substring)
        return input_string[: idx + 0 if also_cut_substring else len(substring)]

    # String -> List of URLs in that string
    @staticmethod
    def extract_urls(input_string: str) -> list[str]:
        regex = r"(http|ftp|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])"
        ret = []
        for match in re.findall(regex, input_string):
            url = match[0] + "://" + "".join(list(match[1:]))
            ret.append(url)
        return ret

    # Retrieve all data from a nested level of a complex object
    # key_list corresponds to nesting of data
    @staticmethod
    def extract_nested_data(data, key_list, extract_to=[]):
        if len(key_list) == 0:
            extract_to.append(data)
            return

        key = key_list[0]
        next_key = key_list[1:]

        if key == "[]":
            if not isinstance(data, list):
                return
            ret = []
            for i in range(len(data)):
                DataWrangler.extract_nested_data(data[i], next_key, extract_to=extract_to)

        else:
            if not isinstance(data, dict):
                return
            if key not in data:
                return
            DataWrangler.extract_nested_data(data[key], next_key, extract_to=extract_to)

        return extract_to


class ShellUtils:
    @staticmethod
    def cd(path: str) -> None:
        os.chdir(path)

    @staticmethod
    # Get list of files in directory
    def ls(path: str) -> list[str]:
        return sorted(os.listdir(path))

    @staticmethod
    # Akin to `find folder`
    def listfiles(folder):
        ret = []
        for root, folders, files in os.walk(folder):
            for filename in folders + files:
                ret.append(os.path.join(root, filename))
        return sorted(ret)

    @staticmethod
    # Copies files
    def cp(src, dest, skip_existing=False):
        if skip_existing and ShellUtils.file_exists(dest):
            return
        ShellUtils.mkparentsdir(dest)
        shutil.copyfile(src, dest)

    @staticmethod
    def mv(src, dest):
        pathlib.Path(src).rename(dest)

    @staticmethod
    def rm(file):
        pathlib.Path(file).unlink()

    @staticmethod
    def rmrf(path, ignore_not_exists=False):
        if ignore_not_exists and not ShellUtils.folder_exists(path):
            return
        shutil.rmtree(path)

    @staticmethod
    # Returns true if it made the directory
    def mkdir(path: str) -> bool:
        if not os.path.exists(path):
            os.mkdir(path)
            return True
        return False

    @staticmethod
    # Make path and parent directories if needed
    def mkdirp(path: str):
        if os.path.exists(path) and os.path.isdir(path):
            return
        pathlib.Path.mkdir(pathlib.Path(path), parents=True)

    @staticmethod
    # Get parent directory in string format:
    def get_parent_dir(path: str):
        x = list(pathlib.Path(path).absolute().parts[:-1])
        parentdir = pathlib.Path(*x)
        return str(parentdir)

    @staticmethod
    # Make parent directories for a path
    def mkparentsdir(path: str):
        x = list(pathlib.Path(path).absolute().parts[:-1])
        parentdir = pathlib.Path(*x)
        if not parentdir.exists():
            parentdir.mkdir(parents=True)

    # Remove an empty directory
    @staticmethod
    def rmdir(path: str):
        os.rmdir(path)

    # Get Absolute Path
    def get_absolute_path(path: str) -> str:
        return os.path.abspath(os.path.expanduser(path))

    @staticmethod
    # Returns true if it made the file
    def touch(path: str) -> bool:
        if not ShellUtils.file_exists(path):
            with open(path, "w") as x:
                pass
            return True
        return False

    # Returns true if file (not directory) exists
    def file_exists(path: str) -> bool:
        if not os.path.exists(path) or not os.path.isfile(path):
            return False
        return True

    def folder_exists(path: str) -> bool:
        if not os.path.exists(path) or not os.path.isdir(path):
            return False
        return True

    def read_binary_file(path: str):
        ret = None
        with open(path, "rb") as bf:
            ret = bf.read()
        return ret

    def write_binary_file(path: str, binarydata):
        ShellUtils.mkparentsdir(path)
        with open(path, "wb") as bf:
            bf.write(binarydata)

    def write_text_to_file(path: str, text: str):
        text = str(text)
        ShellUtils.mkparentsdir(path)
        with open(path, "w", errors="ignore", encoding="utf-8") as f:
            f.write(text)

    def append_text_to_file(path: str, text: str):
        ShellUtils.mkparentsdir(path)
        ShellUtils.touch(path)
        with open(path, "a", errors="ignore", encoding="utf-8") as f:
            f.write(text)

    def read_text_file(path: str):
        ret = ""
        for line in open(path, "r", encoding="utf-8", errors="ignore"):
            ret += line.strip()
            ret += "\n"
        return ret

    def get_filename_from_wholepath(path: str):
        return os.path.basename(path)


class PrintUtils:
    @staticmethod
    def str_and_trim(thing):
        thing = str(thing)
        if len(thing) > 100:
            thing = thing[:100] + "..."
        return thing

    @staticmethod
    def basic_print(thing, offset=0, offset_with="  ", print_generic_types=False) -> None:
        pre = f"{offset_with*offset}"

        def get_ending_as_str(anything):
            return f" [{type(anything).__name__}]\n" if print_generic_types else "\n"

        # Is Unhandled Type?
        if PrintUtils.is_generic(thing):
            print(pre + PrintUtils.str_and_trim(thing), end=get_ending_as_str(thing))
            return

        # Is List
        if isinstance(thing, list):
            for i in range(len(thing)):
                if PrintUtils.is_generic(thing[i]):
                    print(f"{pre}{i}: {thing[i]}")
                else:
                    print(f"{pre}{i}:")
                    PrintUtils.basic_print(
                        thing[i],
                        offset=offset + 1,
                        offset_with=offset_with,
                        print_generic_types=print_generic_types,
                    )

        # Is Dict
        if isinstance(thing, dict):
            for key in thing:
                if PrintUtils.is_generic(thing[key]):
                    print(
                        f"{pre}{str(key)}:",
                        PrintUtils.str_and_trim(thing[key]),
                        end=get_ending_as_str(thing[key]),
                    )
                else:
                    print(f"{pre}{str(key)}:")
                    PrintUtils.basic_print(
                        thing[key],
                        offset=offset + 1,
                        offset_with=offset_with,
                        print_generic_types=print_generic_types,
                    )

    @staticmethod
    def print_model(thing, only_print_leaves=False, model_generics=False, list_counts=False):
        model = PrintUtils.model(thing, model_generics=model_generics, list_counts=list_counts)
        last = ""
        for i in sorted(model):
            if not only_print_leaves:
                print(i)
                continue
            if last not in i:
                print(last)
            last = i
        if only_print_leaves:
            print(last)

    @staticmethod
    def model(thing, path="", description=set(), model_generics=False, list_counts=False):
        if PrintUtils.is_generic(thing):
            if not model_generics:
                return description
            mod = f"_{type(thing).__name__}"
            if path == "":
                path = mod
            else:
                path += "->" + mod
            description.add(path)

        # Is List
        if isinstance(thing, list):
            mod = "[]"
            if list_counts:
                mod = f"[{len(thing)}]"
            if path == "":
                path = mod
            else:
                path += "->" + mod
            description.add(path)
            for i in range(len(thing)):
                PrintUtils.model(
                    thing[i],
                    path=path,
                    description=description,
                    model_generics=model_generics,
                    list_counts=list_counts,
                )

        # Is Dict
        if isinstance(thing, dict):
            for key in thing:
                npath = path
                if npath == "":
                    npath = f"{key}"
                else:
                    npath += f"->{key}"
                description.add(npath)
                PrintUtils.model(
                    thing[key],
                    path=npath,
                    description=description,
                    model_generics=model_generics,
                    list_counts=list_counts,
                )

        return description

    @staticmethod
    def is_generic(thing):
        if isinstance(thing, list):
            return False
        if isinstance(thing, dict):
            return False
        return True


class MiscUtils:
    # bootstrap
    @staticmethod
    def bootstrap_skeleton(dirs=None, files=None):
        if dirs:
            for directory in dirs:
                dirs[directory] = ShellUtils.get_absolute_path(dirs[directory])
                print(dirs[directory])
                ShellUtils.mkdirp(dirs[directory])
        if files:
            for file in files:
                files[file] = ShellUtils.get_absolute_path(files[file])
                ShellUtils.mkparentsdir(files[file])
                ShellUtils.touch(files[file])

    # string -> hash
    @staticmethod
    def hash(string: str):
        return hashlib.sha1(string.encode("utf-8")).hexdigest()

    @staticmethod
    def file_hash(file_path: str, algorithm="md5"):
        hash_object = hashlib.new(algorithm)
        # Open the file in binary mode and read it in chunks
        with open(file_path, "rb") as file:
            while chunk := file.read(8192):  # Read in 8 KB chunks
                hash_object.update(chunk)

        # Get the hexadecimal representation of the hash
        file_hash = hash_object.hexdigest()

        return file_hash

    # iterable --> file
    @staticmethod
    def write_iterable_to_file(file, iterable):
        ShellUtils.write_text_to_file(file, "")
        for i in iterable:
            ShellUtils.append_text_to_file(file, str(i) + "\n")

    # file with strs seperated by newlines and strippable
    @staticmethod
    def read_iterable_from_file(file, encoding="utf-8") -> []:
        ret = []
        if ShellUtils.file_exists(file):
            for line in open(file, "r", encoding=encoding):
                ret.append(line.strip())
        return ret

    # Is Windows?
    @staticmethod
    def is_windows():
        return platform.system().lower() == "windows"

    @staticmethod
    def get_system_identifier():
        os_info = platform.uname()
        os_type = os_info.system
        username = getpass.getuser()
        system = platform.system()

        ret = [system, username]

        ret.append(os_info.node)

        if os_type == "Linux":
            ret.append(platform.freedesktop_os_release()["ID"])
        else:
            ret.append(platform.release())

        return "_".join(ret)

    # Get One Char
    @staticmethod
    def get_char():
        if MiscUtils.is_windows():
            return msvcrt.getch().decode("utf-8")
        else:  # Linux/Unix
            fd = sys.stdin.fileno()
            old_settings = termios.tcgetattr(fd)
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
            return ch


# Internal Classes
class Cache:
    is_active = False
    pre = "southamerica"

    @staticmethod
    def init_cache(path="Cache"):
        path = path.rstrip("/")

        opts = Options.get_instance()
        if Cache.cache_loaded():
            raise RuntimeError("Cannot initialize an already active cache")

        ShellUtils.mkdir(path)
        opts.set("cache_path", path)

        cache_index_file = opts.get("cache_path") + "/index.txt"
        ShellUtils.touch(cache_index_file)
        opts.set("cache_index_file", cache_index_file)

        Cache.load_cache()

    # URI -> key
    @staticmethod
    def get_key(locator: str):
        return hashlib.sha1(f"{Cache.pre}{locator}".encode("utf-8")).hexdigest()

    # key -> URI
    @staticmethod
    def get_locator(key: str):
        opts = Options.get_instance()
        if not Cache.cache_loaded():
            raise RuntimeError("Cannot get a cache locator without an active cache")

        return opts.get("cache_mapper")[key]

    @staticmethod
    def print_cache():
        opts = Options.get_instance()
        if not Cache.cache_loaded():
            raise RuntimeError("Cache is not loaded")

        mapper = opts.get("cache_mapper")
        print(f"The Cache at {opts.get('cache_path')} Currently Contains: {len(mapper)} item(s).")

        for key in mapper:
            print(f"{key} -> {mapper[key]}")

    # Internal methods
    def load_cache():
        opts = Options.get_instance()

        mapper = dict()

        index_file = opts.get("cache_index_file")
        for line in open(index_file, "r", encoding="utf8"):
            line = line.strip()
            if len(line) <= 1:
                continue
            splitat = line.find(" ")
            key = line[:splitat]
            locator = line[splitat + 1 :]
            assert locator not in mapper, f"Duplicate mapping for key: {locator}"
            mapper[locator] = key

        opts.set("cache_mapper", mapper)
        opts.set("cache_loaded", True)
        Cache.is_active = True

    @staticmethod
    def cache_loaded() -> bool:
        opts = Options.get_instance()
        if opts.exists("cache_loaded"):
            return opts.get("cache_loaded")
        return False

    def __contains__(self, locator: str):
        opts = Options.get_instance()
        if not opts.exists("cache_loaded"):
            return False
        return locator in opts.get("cache_mapper")

    @staticmethod
    def cache_item_path(locator: str):
        opts = Options.get_instance()
        return opts.get("cache_path") + "/" + opts.get("cache_mapper")[locator]

    def __getitem__(self, locator: str):
        opts = Options.get_instance()
        if not opts.exists("cache_loaded"):
            raise RuntimeError("Cache not loaded")
        if locator not in self:
            raise RuntimeError(f"Cache does not contain: {locator}")
        if opts.verbose_level >= 10:
            print(f"[CACHE][PULL] {locator}")
        return ShellUtils.read_binary_file(self.cache_item_path(locator))

    def __setitem__(self, locator, binarydata):
        assert not self.__contains__(
            locator
        ), f"Setting item in Cache failed, the following key already exists: {locator}"
        opts = Options.get_instance()
        if not opts.exists("cache_loaded"):
            raise RuntimeError("Cache not loaded")
        mapper = opts.get("cache_mapper")

        cache_item_location = opts.get("cache_path") + "/" + Cache.get_key(locator)
        if opts.verbose_level >= 10:
            print(f"[CACHE][INST] {locator}")
        ShellUtils.write_binary_file(cache_item_location, binarydata)

        index_file = opts.get("cache_index_file")
        ShellUtils.append_text_to_file(index_file, f"{Cache.get_key(locator)} {locator}\n")
        mapper[locator] = Cache.get_key(locator)


class Options:
    __instance = None
    verbose_level = 10
    opts = dict()

    @staticmethod
    def set(key, value):
        Options.get_instance().opts[key] = value

    @staticmethod
    def get(key: str):
        return Options.get_instance().opts[key]

    @staticmethod
    def exists(key: str) -> bool:
        return key in Options.get_instance().opts

    @staticmethod
    def get_instance():
        if Options.__instance == None:
            Options()
        return Options.__instance

    def __init__(self):
        if Options.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            Options.__instance = self


Options()
